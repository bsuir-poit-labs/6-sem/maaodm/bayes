﻿using System;
using System.Windows.Forms;

namespace Bayes
{
    public partial class MainForm : Form
    {
        private readonly Random _random = new();
        private readonly Bayes _bayes = new();

        private const double Eps = 0.001;

        public MainForm()
        {
            InitializeComponent();

            numericUpDown1.Value = (decimal) _random.NextDouble();
            numericUpDown2.Value = 1 - numericUpDown1.Value;
        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            var p1 = (double) numericUpDown1.Value;
            var p2 = (double) numericUpDown2.Value;

            if (p1 < 0 || p1 > 1 || p2 < 0 || p2 > 1)
            {
                MessageBox.Show(@"Значения вероятностей должны быть в пределах от 0 до 1!",
                    @"Неверный ввод данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (Math.Abs(p1 + p2 - 1) > Eps)
            {
                MessageBox.Show(@"Сумма вероятностей должна равняться 1!",
                    @"Неверный ввод данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            pictureBox.Image = _bayes.Execute(p1, p2, pictureBox.Width, pictureBox.Height);
            lbFalseAlarm.Text = $@"{_bayes.FalseAlarm:0.00000}";
            lbMissingDetection.Text = $@"{_bayes.MissingDetection:0.00000}";
            lbClassificationError.Text = $@"{_bayes.ClassificationError:0.00000}";
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown2.Value = 1 - numericUpDown1.Value;
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown1.Value = 1 - numericUpDown2.Value;
        }
    }
}