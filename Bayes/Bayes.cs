﻿using System;
using System.Drawing;
using static System.Math;

namespace Bayes
{
    public class Bayes
    {
        private static readonly Random Random = new();
        private static readonly SolidBrush RedBrush = new(Color.Red);
        private static readonly SolidBrush BlueBrush = new(Color.Blue);
        private static readonly Pen GreenPen = new(Color.Green, 3);

        private const int PointsCount = 10_000;
        private const int Offset = 150;
        private const int Scale = 400_000;
        private const double Eps = 0.001;

        private int _width;
        private int _height;

        public double FalseAlarm { get; private set; }
        public double MissingDetection { get; private set; }
        public double ClassificationError { get; private set; }

        public Bitmap Execute(double p1, double p2, int width, int height)
        {
            _width = width;
            _height = height;

            var bitmap = new Bitmap(_width, _height);
            using var graphics = Graphics.FromImage(bitmap);

            var points1 = GetPoints(_width, -Offset);
            var mu1 = GetExpectedValue(points1);
            var sigma1 = GetStandardDeviation(points1, mu1);
            DrawGraph(mu1, sigma1, p1, RedBrush, graphics);

            var points2 = GetPoints(_width, Offset);
            var mu2 = GetExpectedValue(points2);
            var sigma2 = GetStandardDeviation(points2, mu2);
            DrawGraph(mu2, sigma2, p2, BlueBrush, graphics);

            var borderX = CalcFalseAlarm(p1, p2, mu1, sigma1, mu2, sigma2);
            CalcMissingDetection(borderX, p1, p2, mu1, sigma1);
            ClassificationError = FalseAlarm + MissingDetection;

            graphics.DrawLine(GreenPen, (int) borderX, 0, (int) borderX, _height);

            return bitmap;
        }

        private int[] GetPoints(int width, int offset)
        {
            var points = new int[PointsCount];

            for (int i = 0; i < PointsCount; i++)
            {
                points[i] = Random.Next(width) + offset;
            }

            return points;
        }

        private double GetExpectedValue(int[] points)
        {
            double sum = 0;
            for (int i = 0; i < PointsCount; i++)
            {
                sum += points[i];
            }

            return sum / PointsCount;
        }

        private double GetStandardDeviation(int[] points, double mu)
        {
            double sum = 0;
            for (int i = 0; i < PointsCount; i++)
            {
                sum += Pow(points[i] - mu, 2);
            }

            return Sqrt(sum / PointsCount);
        }

        private void DrawGraph(double mu, double sigma, double p, Brush brush, Graphics graphics)
        {
            for (int x = 0; x < PointsCount; x++)
            {
                var y = Exp(-0.5 * Pow((x - mu) / sigma, 2)) / (sigma * Sqrt(2 * PI));
                graphics.FillRectangle(brush, x, _height - (int) (y * p * Scale), 3, 3);
            }
        }

        private double CalcFalseAlarm(double pc1, double pc2, double mu1, double sigma1, double mu2, double sigma2)
        {
            if (pc1 == 0)
            {
                FalseAlarm = 1;
                return 0;
            }

            if (pc2 == 0)
            {
                FalseAlarm = 0;
                return 0;
            }

            double x = -Offset;
            var p1 = 1.0;
            var p2 = 0.0;
            double falseAlarmError = 0;

            if (pc2 != 0)
                while (p2 < p1)
                {
                    p1 = pc1 * Exp(-0.5 * Pow((x - mu1) / sigma1, 2)) / (sigma1 * Sqrt(2 * PI));
                    p2 = pc2 * Exp(-0.5 * Pow((x - mu2) / sigma2, 2)) / (sigma2 * Sqrt(2 * PI));
                    falseAlarmError += p2 * Eps;
                    x += Eps;
                }

            FalseAlarm = falseAlarmError;
            return x;
        }

        private void CalcMissingDetection(double borderX, double pc1, double pc2, double mu1, double sigma1)
        {
            if (pc1 == 0)
            {
                MissingDetection = 0;
                return;
            }

            if (pc2 == 0)
            {
                MissingDetection = 0;
                return;
            }

            double missingDetectingError = 0;
            var x = borderX;
            while (x < _width + Offset)
            {
                var p1 = Exp(-0.5 * Pow((x - mu1) / sigma1, 2)) / (sigma1 * Sqrt(2 * PI));
                missingDetectingError += p1 * pc1 * Eps;
                x += Eps;
            }

            MissingDetection = missingDetectingError;
        }
    }
}