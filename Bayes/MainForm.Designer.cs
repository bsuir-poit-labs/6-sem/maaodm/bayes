﻿namespace Bayes
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ToolPanel = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.lbFalseAlarm = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbMissingDetection = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbClassificationError = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lbP1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.lbP2 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.executeButton = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.ToolPanel.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolPanel
            // 
            this.ToolPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ToolPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ToolPanel.Controls.Add(this.flowLayoutPanel2);
            this.ToolPanel.Controls.Add(this.flowLayoutPanel1);
            this.ToolPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.ToolPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.ToolPanel.Location = new System.Drawing.Point(1110, 0);
            this.ToolPanel.Name = "ToolPanel";
            this.ToolPanel.Size = new System.Drawing.Size(372, 853);
            this.ToolPanel.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.label1);
            this.flowLayoutPanel2.Controls.Add(this.lbFalseAlarm);
            this.flowLayoutPanel2.Controls.Add(this.label3);
            this.flowLayoutPanel2.Controls.Add(this.lbMissingDetection);
            this.flowLayoutPanel2.Controls.Add(this.label5);
            this.flowLayoutPanel2.Controls.Add(this.lbClassificationError);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 634);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(370, 217);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.Location = new System.Drawing.Point(7, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(313, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Вероятность ложной тревоги:";
            // 
            // lbFalseAlarm
            // 
            this.lbFalseAlarm.Location = new System.Drawing.Point(35, 26);
            this.lbFalseAlarm.Margin = new System.Windows.Forms.Padding(35, 0, 3, 14);
            this.lbFalseAlarm.Name = "lbFalseAlarm";
            this.lbFalseAlarm.Size = new System.Drawing.Size(285, 23);
            this.lbFalseAlarm.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.Location = new System.Drawing.Point(7, 63);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(361, 33);
            this.label3.TabIndex = 2;
            this.label3.Text = "Вероятность пропуска обнаружения:";
            // 
            // lbMissingDetection
            // 
            this.lbMissingDetection.Location = new System.Drawing.Point(35, 96);
            this.lbMissingDetection.Margin = new System.Windows.Forms.Padding(35, 0, 3, 14);
            this.lbMissingDetection.Name = "lbMissingDetection";
            this.lbMissingDetection.Size = new System.Drawing.Size(285, 23);
            this.lbMissingDetection.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoEllipsis = true;
            this.label5.Location = new System.Drawing.Point(7, 133);
            this.label5.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(348, 37);
            this.label5.TabIndex = 4;
            this.label5.Text = "Суммарная ошибка классификации:";
            // 
            // lbClassificationError
            // 
            this.lbClassificationError.Location = new System.Drawing.Point(35, 170);
            this.lbClassificationError.Margin = new System.Windows.Forms.Padding(35, 0, 3, 14);
            this.lbClassificationError.Name = "lbClassificationError";
            this.lbClassificationError.Size = new System.Drawing.Size(285, 23);
            this.lbClassificationError.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.lbP1);
            this.flowLayoutPanel1.Controls.Add(this.numericUpDown1);
            this.flowLayoutPanel1.Controls.Add(this.lbP2);
            this.flowLayoutPanel1.Controls.Add(this.numericUpDown2);
            this.flowLayoutPanel1.Controls.Add(this.executeButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(7, 14, 7, 14);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(370, 151);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // lbP1
            // 
            this.lbP1.Location = new System.Drawing.Point(12, 28);
            this.lbP1.Margin = new System.Windows.Forms.Padding(5, 14, 0, 0);
            this.lbP1.Name = "lbP1";
            this.lbP1.Size = new System.Drawing.Size(69, 30);
            this.lbP1.TabIndex = 0;
            this.lbP1.Text = "P1(C1)";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 2;
            this.numericUpDown1.Increment = new decimal(new int[] {1, 0, 0, 131072});
            this.numericUpDown1.Location = new System.Drawing.Point(88, 28);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(7, 14, 15, 0);
            this.numericUpDown1.Maximum = new decimal(new int[] {1, 0, 0, 0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(84, 34);
            this.numericUpDown1.TabIndex = 1;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // lbP2
            // 
            this.lbP2.Location = new System.Drawing.Point(194, 28);
            this.lbP2.Margin = new System.Windows.Forms.Padding(7, 14, 0, 0);
            this.lbP2.Name = "lbP2";
            this.lbP2.Size = new System.Drawing.Size(69, 30);
            this.lbP2.TabIndex = 2;
            this.lbP2.Text = "P2(C2)";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.DecimalPlaces = 2;
            this.numericUpDown2.Increment = new decimal(new int[] {1, 0, 0, 131072});
            this.numericUpDown2.Location = new System.Drawing.Point(270, 28);
            this.numericUpDown2.Margin = new System.Windows.Forms.Padding(7, 14, 7, 0);
            this.numericUpDown2.Maximum = new decimal(new int[] {1, 0, 0, 0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(85, 34);
            this.numericUpDown2.TabIndex = 3;
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // executeButton
            // 
            this.executeButton.Location = new System.Drawing.Point(124, 97);
            this.executeButton.Margin = new System.Windows.Forms.Padding(117, 35, 117, 3);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(137, 37);
            this.executeButton.TabIndex = 0;
            this.executeButton.Text = "Построить";
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1110, 853);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1482, 853);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.ToolPanel);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Критерий Байеса";
            this.ToolPanel.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).EndInit();
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbMissingDetection;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbClassificationError;

        private System.Windows.Forms.Label lbFalseAlarm;

        private System.Windows.Forms.Label label1;

        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.Label lbP2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;

        private System.Windows.Forms.NumericUpDown numericUpDown1;

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;

        private System.Windows.Forms.Label lbP1;

        private System.Windows.Forms.PictureBox pictureBox;

        private System.Windows.Forms.Panel ToolPanel;

        #endregion
    }
}